<?php
/*
  Ce fichier provient du repository bitbucketci-wordpress
*/
namespace Deployer;

set('default_timeout', 7200);
set('keep_releases', '4');
set('drupal_path', 'web');
set('drush_cmd', '{{bin/php}} {{release_path}}/vendor/drush/drush/drush');

set('shared_files', [
  '{{drupal_path}}/sites/{{drupal_site}}/settings.env.php',
]);

set('shared_dirs', [
  '{{drupal_path}}/sites/{{drupal_site}}/files',
  '{{drupal_path}}/sites/{{drupal_site}}/private',
  '{{drupal_path}}/sites/{{drupal_site}}/tmp',
]);

set('writable_dirs', [
  '{{drupal_path}}/sites/{{drupal_site}}/files',
  '{{drupal_path}}/sites/{{drupal_site}}/private',
  '{{drupal_path}}/sites/{{drupal_site}}/tmp'
]);
set('allow_anonymous_stats', false);