<?php
/*
  Ce fichier provient du repository bitbucketci-wordpress
*/
namespace Deployer;
use Deployer\Task\Context;  
require __DIR__ . '/sets.php';



task('cachetool:clear:opcache', function () {
    run("if [ ! -f {{deploy_path}}/cachetool.phar ]; then cd {{deploy_path}}/; curl -sO https://gordalina.github.io/cachetool/downloads/cachetool-4.1.1.phar && mv cachetool-4.1.1.phar cachetool.phar && chmod +x cachetool.phar ; fi");
    run("cd {{release_path}} && php {{deploy_path}}/cachetool.phar opcache:reset --fcgi={{cachetool}}");
});

desc('Setup');
task('deploy:setup', function() {
  run('ln -s {{deploy_path}}/tmp/ {{deploy_path}}/current ');
});

desc('Cron drupal configs sync');
task('drupal:cron:drupal_configs', function() {
  $BITBUCKET_BRANCH = runLocally('echo $BITBUCKET_BRANCH');
  $DRUPAL_CONFIG_ENV_REF = runLocally('echo $DRUPAL_CONFIG_ENV_REF');
  $AWS_LAMBDA_API_GATEWAY_KEY = runLocally('echo $AWS_LAMBDA_API_GATEWAY_KEY');
  if($BITBUCKET_BRANCH == $DRUPAL_CONFIG_ENV_REF){
    writeln("Installation du cron de gestion des drupals configs.");
    run('if [ ! -d "{{release_path}}/scripts/crons/" ]; then mkdir -p {{release_path}}/scripts/crons/; fi');
    run('cp {{release_path}}/pipelines/deploy/misc/cron-drupal-configs-sync.sh {{release_path}}/scripts/crons/');
    run('sed -E -i "s|INSIGN_REPOSITORY_SLUG|'.$INSIGN_REPOSITORY_SLUG.'|" {{release_path}}/scripts/crons/cron-drupal-configs-sync.sh');
    run('sed -E -i "s|AWS_LAMBDA_API_GATEWAY_KEY|'.$AWS_LAMBDA_API_GATEWAY_KEY.'|" {{release_path}}/scripts/crons/cron-drupal-configs-sync.sh');
    run('sed -E -i "s|DRUPAL_CONFIG_ENV_REF_VALUE|'.$DRUPAL_CONFIG_ENV_REF.'|" {{release_path}}/scripts/crons/cron-drupal-configs-sync.sh');
    run('crontab -l | grep -v "insign_drupal_configs"  | crontab -');
    run('echo "*/15 * * * * cd {{deploy_path}}/current/ && ./scripts/crons/cron-drupal-configs-sync.sh -l {{drupal_site}} # insign_drupal_configs" | crontab -');
  } else {
    writeln("L'environnement de la branche " . runLocally('echo $BITBUCKET_BRANCH'). " n'est pas l'environnement de référence pour les configs Drupal : pas de cron à installer.");
  }
});

desc('Make and deploy artifact via rsync');
task('deploy:artifact', function() {
  $branch_deployed = runLocally('echo $BITBUCKET_BRANCH');
  if ($branch_deployed != "") {
    runLocally('echo '.$branch_deployed.' > ../../../.branch_deployed;');
  }
  runLocally('tar -zcvf /tmp/artifact.tgz -C ../../../ .');
  upload('/tmp/artifact.tgz','{{release_path}}/artifact.tgz');
  run('tar -zxvf {{release_path}}/artifact.tgz -C {{release_path}}/ --strip 1');
  run('rm -f {{release_path}}/artifact.tgz');
}
);

/*
* A utiliser lorsque le serveur cible n'a pas rsync
*/
desc('Make and deploy artifact via scp');
task('deploy:artifact:scp', function() {
   runLocally('tar -zcvf /tmp/artifact.tgz -C ../../../ .');
   runLocally('scp /tmp/artifact.tgz '. Context::get()->getHost() . ':{{release_path}}');
   run('tar -zxvf {{release_path}}/artifact.tgz -C {{release_path}}/ --strip 1');
   run('rm -f {{release_path}}/artifact.tgz');
}
);

desc('Backup database');
task('deploy:database:backup', function() {
    upload('../misc/deployer-mysql-dump.sh','{{deploy_path}}/deployer-mysql-dump.sh; cd {{deploy_path}}/; chmod +x deployer-mysql-dump.sh;');
    run('cd {{deploy_path}}/; ./deployer-mysql-dump.sh {{deploy_path}} {{deploy_path}}/current/');
}
)->onRoles('prod');

desc('Clean redis cache');
task('deploy:redis:flushall', function() {
    run('cd {{release_path}}; redis-cli FLUSHALL');
}
);

desc('Composer install');
task('composer:19:install', function () {
    run("if [ ! -f {{deploy_path}}/composer.phar ]; then cd {{deploy_path}}; curl https://getcomposer.org/installer | /usr/bin/php ; fi");
    run("/usr/bin/php {{deploy_path}}/composer.phar self-update 1.9.1");
    run("cd {{release_path}}/; /usr/bin/php {{deploy_path}}/composer.phar install --no-dev --no-progress --prefer-dist");
});

task('deploy:cache:clear', function () {
  run("cd {{release_path}} && {{bin/php}} -f shell/uncache.php flushall flushsystem cleanmedia massrefresh varnishcacheclean");
})->desc('Clear cache');

task('drupal:status', function () {
  $output = run("if [ -L '{{deploy_path}}/current' ] && [ -f {{deploy_path}}/current/vendor/bin/drush ]; then cd {{deploy_path}}/current && {{drush_cmd}} -l {{drupal_site}} status 2>&1; fi");
  writeln($output);
});
task('drupal:maintenance:on', function () {
  $output = run("if [ -L '{{deploy_path}}/current' ] && [ -f {{deploy_path}}/current/vendor/bin/drush ]; then cd {{deploy_path}}/current && {{drush_cmd}} -l {{drupal_site}} state-set --yes system.maintenance_mode 1 2>&1; fi");
  writeln($output);
});
task('drupal:maintenance:off', function () {
  $output = run("if [ -L '{{deploy_path}}/current' ] && [ -f {{deploy_path}}/current/vendor/bin/drush ]; then cd {{deploy_path}}/current && {{drush_cmd}} -l {{drupal_site}} state-set --yes system.maintenance_mode 0 2>&1; fi");
  writeln($output);
});
task('drupal:cache:rebuild', function () {
  $output = run("if [ -L '{{deploy_path}}/current' ] && [ -f {{deploy_path}}/current/vendor/bin/drush ]; then cd {{deploy_path}}/current && {{drush_cmd}} -l {{drupal_site}} --yes cache-rebuild 2>&1; fi");
  writeln($output);
});
task('drupal:updatedb', function () {
  $output = run("if [ -L '{{deploy_path}}/current' ] && [ -f {{deploy_path}}/current/vendor/bin/drush ]; then cd {{deploy_path}}/current && {{drush_cmd}} -l {{drupal_site}} --yes updatedb 2>&1; fi");
  writeln($output);
});
task('drupal:config:import', function () {
  $output = run("if [ -L '{{deploy_path}}/current' ] && [ -f {{deploy_path}}/current/vendor/bin/drush ]; then cd {{deploy_path}}/current && {{drush_cmd}} -l {{drupal_site}} --no-interaction config-import 2>&1; fi");
  writeln($output);
});

desc('Test');
task('deploye:test', function () {
    echo $ent->getHostname();
});

task('phackage:reset:profile', function () {
  $output = run(
    "if [ -L '{{deploy_path}}/current' ]; then ".
      "cd {{deploy_path}}/current;".
      "{{drush_cmd}} -l {{drupal_site}} sql-drop -y 2>&1;".
      "{{drush_cmd}} -l {{drupal_site}} si pink --site-name=PHackage --account-pass={{admin_account_pass}} --account-name={{admin_account_login}} --site-mail={{site_mail}} --account-mail={{admin_account_mail}} -y 2>&1;".
    "fi"
  );
  writeln($output);
});
task('phackage:enable-all-features', function () {
  $output = run(
    "if [ -L '{{deploy_path}}/current' ]; then ".
    "cd {{deploy_path}}/current;".
    "{{drush_cmd}} en {{phackage_all_features}} -l {{drupal_site}} -y 2>&1;".
    "fi"
  );
  writeln($output);
});