<?php
/*
  Ce fichier provient du repository bitbucketci-wordpress
*/
namespace Deployer;
require __DIR__ . '/tasks.php';

desc('Deploy release on web servers');
task(
    'deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:release',
    'deploy:artifact',
    'deploy:shared',
    'deploy:writable',
    // For real projects.
    'drupal:maintenance:on',
    'deploy:symlink',
    'drupal:status',
    // To be removed for real projects.
    //'phackage:reset:profile',
    //'phackage:enable-all-features',
    // For real projects.
    'drupal:cache:rebuild',
    'drupal:updatedb',
    'drupal:config:import',
    'drupal:cron:drupal_configs',
    // For real projects.
    'drupal:maintenance:off',
    'cleanup',
    'success'
    ]
);

