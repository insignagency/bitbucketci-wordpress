#!/bin/bash
#
#  Ce fichier provient du repository bitbucketci-wordpress
#
set -e
set -x

cd ./pipelines/deploy/misc/
infos="$(php get-host-info.php $SYNC_CI_DB_FROM)"
# on n'exporte pas les variables qui ont un nom interdit, contenant un slash : bin/php=php7.4
for var_exp in $infos; do
    if [[ ! "$var_exp" =~ .+/.+= ]]; then
    eval $var_exp
  fi
done

# On teste si le cms a déjà été déployé
env_status=$(ssh -p $port $user@$host "if [ -L $deploy_path/current ]; then echo 'deployed'; fi")
if [ "$env_status" == "deployed" ]; then
  echo "L'envitonnement distant n'a pas de symlink current : on ne synchronise pas la bdd de la CI."
else
  ./sync-ci-db.sh $SYNC_CI_DB_FROM
fi

cd ../../../
./pipelines/build/default/composer-version.sh
./pipelines/build/default/install-cms.sh